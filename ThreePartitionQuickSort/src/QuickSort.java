
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class QuickSort {
	private static AtomicInteger numberOfThreads = new AtomicInteger(0);
	private static AtomicInteger maximumNumberOfThreads = new AtomicInteger(0);
	
	public static void sort(Comparable [] array, int number){
		maximumNumberOfThreads.set(number);
		qsort(array, 0, array.length-1);
	}
	
	private static void qsort(Comparable [] array, int beginIndex, int lastIndex){
		if(lastIndex-beginIndex<=0)return;
		int pomIndex=beginIndex;
		Comparable pom;
		int tail=beginIndex;
		int head=beginIndex++;
		while(beginIndex<=lastIndex){
			if(array[beginIndex].compareTo(array[tail])==0){
				++head;
				pom=array[beginIndex];
				array[beginIndex]=array[head];
				array[head]=pom;
			}
			else if(array[beginIndex].compareTo(array[tail])<0){
				pom=array[tail];
				array[tail++]=array[beginIndex];
				array[beginIndex]=pom;
				pom=array[++head];
				array[head]=array[beginIndex];
				array[beginIndex]=pom;
			}
			++beginIndex;
		}
		int taill=--tail;
		int headd=++head;
		if(numberOfThreads.get()<maximumNumberOfThreads.get()){
			Thread t1, t2;
			t1=new Thread(
					new Runnable(){
						public void run(){
							numberOfThreads.incrementAndGet();
							qsort(array, pomIndex, taill);
						}
					}
					);
			t2=new Thread(
					new Runnable(){
						public void run(){
							numberOfThreads.incrementAndGet();
							qsort(array, headd, lastIndex);
						}
					}
					);
			t1.setPriority(Thread.MAX_PRIORITY);
			t2.setPriority(Thread.MAX_PRIORITY);
			t1.start();
			t2.start();
			try{
			t1.join();
			t2.join();
			numberOfThreads.decrementAndGet();
			numberOfThreads.decrementAndGet();
			}
			catch(Exception e){
				
			}
			
		}
		else{
			qsort(array, pomIndex, taill);
			qsort(array, headd, lastIndex);
			
		}
	}
	
	public static void main(String[] args) {
		Integer tab[] = new Integer[10000000];
		Integer pom[] = new Integer[10000000];
		
		Random generator = new Random();
		for(int i=0; i<10000000; ++i){
			pom[i]=tab[i]=generator.nextInt();
			
		}
		// TODO Auto-generated method stub
		//Integer [] tab={1,6,4,7,3,5,7,9,7,4,2,5,5,8, 4,3,2,1,1,1,3,4,6,7,3};
		long dupa;
		System.out.println("Zaczynam własne sortowanie");
		dupa=System.nanoTime();
		sort(tab, 2);
		System.out.println("Kończę własne sortowanie"+(System.nanoTime()-dupa));
		System.out.println("Zaczynam domyślne sortowanie");
		dupa=System.nanoTime();
		Arrays.sort(pom);
		System.out.println("Kończę domyślne sortowanie"+(System.nanoTime()-dupa));
		/*for(Integer i:tab){
			System.out.println(i);
		}*/

	}

}
